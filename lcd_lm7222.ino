
#define PINS 8
int PIN[PINS] = {13,12,11,10,9,8,7,6};
/*
#define PINS 4
 int PIN[PINS] = {9,8,7,6};
 */

#define E    2 
#define RW   4
#define DI   5

/*#define CS3  10 
#define CS2  11
#define CS1  12
#define RST  13
*/

#define VNEG 3

#define DISPLAY_ON  0b00111001
#define DISPLAY_OFF 0b00111000

int val,in;                           //здесь будет храниться принятый символ




void setup(){
  Serial.begin(9600);
  tone(VNEG,2000);

  for (int i=0; i < PINS ; ++i) {
    pinMode(PIN[i],OUTPUT);
  }

  pinMode(RW,OUTPUT);
  pinMode(E,OUTPUT);
  pinMode(DI,OUTPUT);
  /*
  pinMode(CS1,OUTPUT);
  pinMode(CS2,OUTPUT);
  pinMode(CS3,OUTPUT);
  pinMode(RST,OUTPUT);
  
  digitalWrite(CS1,LOW);
   digitalWrite(CS2,HIGH);
   digitalWrite(CS3,HIGH);
   
   digitalWrite(RST,LOW);
   digitalWrite(RST,HIGH);
   */
}

void loop(){

  if (Serial.available()) {         //если есть принятый символ,
    val = Serial.read();            //  то читаем его и сохраняем в val
    if (val == 'H' or val == 'h') {    
      //    lcd_display_on();     //  если принят симовол 'H',...
      lcd_put_instruction(DISPLAY_ON);
      Serial.println (lcd_read_status(),BIN);
    }
    if (val == 'L' or val == 'l') {               //  если принят симовол 'L',
      lcd_put_instruction(DISPLAY_OFF); 
      Serial.println (lcd_read_status(),BIN);  
    }

    if (val == 'P' or val == 'p') {               //  если принят симовол 'P',
      for (int a=0; a < 178; a++) {
      lcd_set_address(a);
      lcd_putdata(255);
      }  
      Serial.println (lcd_read_status(),BIN);  
    }

    if (val == 'C' or val == 'c') {               //  если принят симовол 'C',
        for (int a=0; a < 178; a++) {
      lcd_set_address(a);
      lcd_putdata(0);
      } 
      Serial.println (lcd_read_status(),BIN);  
    }

  }

} 


void lcd_latch () {
  digitalWrite(E,LOW);
  digitalWrite(E,HIGH);
  digitalWrite(E,LOW);
}

byte lcd_read_status () {
  byte value=0;
  //Set port in to the INPUT mode
  for (int i=0; i < PINS ; ++i) {
    pinMode(PIN[i],INPUT);
  }

  digitalWrite(DI,LOW);
  digitalWrite(RW,HIGH); 
  digitalWrite(E,HIGH); 
  delay(1);
  for (int i=0; i < PINS ; ++i) {
    bitWrite(value,i,digitalRead(PIN[i]));
  }
  delay(1);
  digitalWrite(E,LOW); 
  digitalWrite(RW,LOW);   
  //Set port in to the OUTPUT mode
  for (int i=0; i < PINS ; ++i) {
    pinMode(PIN[i],OUTPUT);
  }
  return value;
}


void lcd_set_data (byte num) {
  for (int i=0; i < PINS ; ++i) {
    digitalWrite(PIN[i],bitRead(num,i));
  }
  Serial.println(num,BIN);
}


void lcd_display_mode (byte mode) {
  digitalWrite(DI,LOW);
  digitalWrite(RW,LOW);
  digitalWrite(E,HIGH);
  lcd_set_data (mode);
  digitalWrite(E,LOW);

}


void lcd_set_address (byte addr) {
  digitalWrite(DI,LOW);
  digitalWrite(RW,LOW);
  digitalWrite(E,HIGH);
  lcd_set_data (addr);
  digitalWrite(E,LOW);
  digitalWrite(RW,HIGH);

}

void lcd_putdata (byte data) {
  digitalWrite(RW,LOW);
  digitalWrite(DI,HIGH);
  digitalWrite(E,HIGH);
  lcd_set_data (data);
  digitalWrite(E,LOW);
  digitalWrite(DI,LOW);
  digitalWrite(RW,HIGH);
}

void lcd_put_instruction (byte data) {
  digitalWrite(RW,LOW);
  digitalWrite(DI,LOW);
  digitalWrite(E,HIGH);
  lcd_set_data (data);
  digitalWrite(E,LOW);
  digitalWrite(DI,LOW);
  digitalWrite(RW,LOW);
}

void lcd_display_on() {
  byte num = DISPLAY_ON;
  digitalWrite(RW,LOW);
  digitalWrite(DI,HIGH);
  digitalWrite(E,HIGH);
  for (int i=0; i < PINS ; ++i) {
    digitalWrite(PIN[i],bitRead(num,i));
  }
  Serial.print("num=");
  Serial.println(num,BIN);
  digitalWrite(E,LOW);

  digitalWrite(E,HIGH);
  num&=0x0F;
  for (int i=0; i < PINS ; ++i) {
    digitalWrite(PIN[i],bitRead(num,i));
  }
  Serial.print("num(low)=");
  Serial.println(num,BIN);
  digitalWrite(E,LOW);
  digitalWrite(DI,LOW);
  digitalWrite(RW,LOW);
}



